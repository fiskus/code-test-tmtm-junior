function main () {
  var panelsElements = document.querySelector('.wrapper');
  [].slice.apply(panelsElements.children).forEach(function (panelElement) {
    var panel = new Panel();
    panel.init(panelElement);
  });
}

window.onload = main;


var panelToCounter = {
  'blue': '.blue-counter',
  'green': '.green-counter',
  'red': '.red-counter'
};

var Panel = function() {};

Panel.prototype.init = function (container) {
  var counterClassName = panelToCounter[container.className];
  this._counterElement = document.querySelector(counterClassName);

  container.addEventListener('click', this.onClick.bind(this));
};

Panel.prototype.onClick = function () {
  var currentCount = Number(this._counterElement.textContent);
  currentCount++;
  this._counterElement.textContent = currentCount;
};
